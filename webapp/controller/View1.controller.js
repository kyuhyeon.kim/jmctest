sap.ui.define([
    "sap/ui/core/mvc/Controller"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller) {
        "use strict";
        
        return Controller.extend("jmctest.controller.View1", {
            onInit: function () {
                $.ajax({
                    url: this.getOwnerComponent().getManifestObject().resolveUri("c4c/OpportunityCollection"),
                    method: "GET",
                    success: function (e) {
                        debugger
                        console.log(e)
                        return e
                    },
                    error: function (e) {
                        debugger
                        console.log(e)
                        return e
                    }
                })
            }
        });
    });
