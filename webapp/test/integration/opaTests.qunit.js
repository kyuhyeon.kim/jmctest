/* global QUnit */

sap.ui.require(["jmctest/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
