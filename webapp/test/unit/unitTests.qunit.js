/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"jmctest/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});
